<?php

namespace App\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function login(AuthenticationUtils $authentication_utils)
    {
        $user = $this->getUser();

        return new JsonResponse([
            'status' => 'success',
            'message' => 'Successful login request',
            'name' => $user->getFirstname(),
        ]);
    }

    public function logout()
    {
        throw new \LogicException();
    }
}