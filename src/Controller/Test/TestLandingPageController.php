<?php


namespace App\Controller\Test;


use Symfony\Component\HttpFoundation\Response;

class TestLandingPageController
{
    public function land(): Response
    {
        return new Response('<html><body><h1>Landing Page</h1></body></html>');
    }
}