<?php

namespace App\Controller\Test;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/api/test/{id}", methods={"GET"})
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getTest(int $id): JsonResponse
    {
        return new JsonResponse([
            'status' => 'success',
            'message' => 'Successful GET request',
            'id' => $id
        ]);
    }

    /**
     * @Route("/api/test", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postTest(Request $request): JsonResponse
    {
        $request_data = json_decode($request->getContent());

        return new JsonResponse([
            'status' => 'success',
            'message' => 'Successful POST request',
            'id' => 1,
            'name' => $request_data->name
        ]);
    }

    /**
     * @Route("/api/test/{id}", methods={"PUT"})
     *
     * @param int $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function putTest(int $id, Request $request): JsonResponse
    {
        $request_data = json_decode($request->getContent());

        return new JsonResponse([
            'status' => 'success',
            'message' => 'Successful PUT request',
            'id' => $id,
            'name' => $request_data->name
        ]);
    }

    /**
     * @Route("/api/test/{id}", methods={"DELETE"})
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deleteTest(int $id): JsonResponse
    {
        return new JsonResponse([
            'status' => 'success',
            'message' => 'Successful DELETE request',
            'id' => $id
        ]);
    }
}