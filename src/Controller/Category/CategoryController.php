<?php

namespace App\Controller\Category;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * Get all categories.
     *
     * This will return all categories.
     *
     * @Route("/api/categories", methods={"GET"})
     *
     * @param CategoryRepository $category_repository
     *
     * @return JsonResponse
     */
    public function getCategories(CategoryRepository $category_repository): JsonResponse
    {
        //Get all categories.
        $categories = $category_repository->findAll();

        if ($categories) {
            return $this->json($categories);
        }

        return $this->json($categories, Response::HTTP_NOT_FOUND);
    }

    /**
     * Get a category.
     *
     * This will return a single category if it matches the specified ID.
     *
     * @Route("/api/categories/{id}", methods={"GET"})
     *
     * @param int $id
     * @param CategoryRepository $category_repository
     *
     * @return JsonResponse
     */
    public function getCategory(int $id,
                                CategoryRepository $category_repository): JsonResponse
    {
        //Get the defined category.
        $category = $category_repository->find($id);

        if ($category) {
            return $this->json($category);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }

    /**
     * Create a category.
     *
     * This will create a new category using the data supplied.
     *
     * @Route("/api/categories", methods={"POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     *
     * @return JsonResponse
     */
    public function createCategory(Request $request,
                                   EntityManagerInterface $entity_manager): JsonResponse
    {
        //Decode the request content.
        $request_data = json_decode(($request->getContent()));

        //Do not proceed if a name (required) is not received.
        if (!isset($request_data->name)) {
            return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Create the new category.
        $category = new Category();
        $category->setName($request_data->name);
        $category->setDescription(isset($request_data->description) ? $request_data->description : null);

        $entity_manager->persist($category);
        $entity_manager->flush();

        return $this->json($category, Response::HTTP_CREATED);
    }

    /**
     * Replace a category.
     *
     * This will replace a categories fields with the data supplied.
     * Data for all of the required fields must be sent for this function to execute.
     * If only a subset of the required fields are to be updated, the PATCH endpoint should be used instead.
     *
     * @Route("/api/categories/{id}", methods={"PUT"})
     *
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param CategoryRepository $category_repository
     *
     * @return JsonResponse
     */
    public function replaceCategory(int $id,
                                    Request $request,
                                    EntityManagerInterface $entity_manager,
                                    CategoryRepository $category_repository): JsonResponse
    {
        $category = $category_repository->find($id);

        //Check the category exists.
        if ($category) {
            //Decode the request content.
            $request_data = json_decode(($request->getContent()));

            //Do not proceed if a name (required) is not received.
            if (!isset($request_data->name)) {
                return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $category->setName($request_data->name);
            $category->setDescription(isset($request_data->description) ? $request_data->description : null);

            $entity_manager->flush();

            return $this->json($category);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }

    /**
     * Update a category.
     *
     * This will update a categories fields with the data supplied.
     * This does not require data for all of the required fields and can update a subset of the fields.
     * If all of the fields are to be updated, the PUT endpoint would be better suited.
     *
     * @Route("/api/categories/{id}", methods={"PATCH"})
     *
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entity_manager
     * @param CategoryRepository $category_repository
     *
     * @return JsonResponse
     */
    public function updateCategory(int $id,
                                   Request $request,
                                   EntityManagerInterface $entity_manager,
                                   CategoryRepository $category_repository): JsonResponse
    {
        $category = $category_repository->find($id);

        //Check the category exists.
        if ($category) {
            //Decode the request content.
            $request_data = json_decode(($request->getContent()));

            //Process all of the fields received.
            foreach ($request_data as $key => $data) {
                if ($key === 'name') {
                    $category->setName($data);
                }
                if ($key === 'description') {
                    $category->setDescription($data);
                }
            }

            $entity_manager->flush();

            return $this->json($category);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }

    /**
     * Delete a category.
     *
     * This will delete a single category if it matches the specified ID.
     *
     * @Route("/api/categories/{id}", methods={"DELETE"})
     *
     * @param int $id
     * @param CategoryRepository $category_repository
     * @param EntityManagerInterface $entity_manager
     *
     * @return JsonResponse
     */
    public function deleteCategory(int $id,
                                   CategoryRepository $category_repository,
                                   EntityManagerInterface $entity_manager): JsonResponse
    {
        //Make sure the category exists.
        $category = $category_repository->find($id);

        if ($category) {
            //Remove the category.
            $entity_manager->remove($category);
            $entity_manager->flush();

            return $this->json(null, Response::HTTP_NO_CONTENT);
        }

        return $this->json($category, Response::HTTP_NOT_FOUND);
    }
}